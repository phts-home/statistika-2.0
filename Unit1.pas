unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Menus, ShellApi;

const
  WM_MYICONNOTIFY = WM_USER + 123;

type
  TForm1 = class(TForm)
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    ListBox1: TListBox;
    N3: TMenuItem;
    N4: TMenuItem;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Image1: TImage;
    Label7: TLabel;
    Timer1: TTimer;
    Label3: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    N5: TMenuItem;
    N6: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure Label7Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure Timer1Timer(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure Label9Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
  private

    { Private declarations }

  public
    ReportFile: String;
    procedure WMICON(var msg: TMessage); message WM_MYICONNOTIFY;
    procedure RestoreMainForm;
    procedure HideMainForm;
    procedure CreateTrayIcon(n:Integer);
    procedure DeleteTrayIcon(n:Integer);

    { Public declarations }
  end;

var
  Form1: TForm1;
  s:string;
  f1:textfile;
  CClose:boolean;
  P : TPoint;

implementation

{$R *.dfm}

procedure Reading;
begin
try Form1.ListBox1.Items.LoadFromFile(Form1.ReportFile)
  except
    begin
    AssignFile(f1,Form1.ReportFile);
    Rewrite(f1);
    CloseFile(f1);
    Form1.ListBox1.Items.LoadFromFile(Form1.ReportFile);
    end;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
cclose:=true;
CreateTrayIcon(1);

application.ShowMainForm:=false;
ReportFile:=ExtractFilePath(ParamStr(0))+'Report.txt';

Reading;

ListBox1.Items.Add(datetostr(date)+' '+timetostr(time));
ListBox1.Items.SaveToFile(ReportFile);
ListBox1.Items.Clear;
end;

procedure TForm1.N2Click(Sender: TObject);
begin
Reading;
s:=ListBox1.Items[ListBox1.Items.Count-1];
ListBox1.Items.Clear;
ListBox1.Items.Add(s);
ListBox1.Items.SaveToFile(ReportFile);
ListBox1.Items.Clear;
end;

procedure TForm1.N3Click(Sender: TObject);
begin
Form1.Left:=p.X-200;
Form1.Top:=p.Y-170;
RestoreMainForm;
end;

procedure TForm1.WMICON(var msg: TMessage);
begin
case msg.LParam of
  WM_RBUTTONDOWN :
              begin
              GetCursorPos(p);
              SetForegroundWindow(Application.MainForm.Handle);
              PopupMenu1.Popup(P.X, P.Y);
              end;
  WM_LBUTTONDBLCLK : RestoreMainForm;
  end;
end;

procedure TForm1.HideMainForm;
begin
  form1.Timer1.Enabled:=false;
  cclose:=true;
  Application.ShowMainForm := False;
  ShowWindow(Application.Handle, SW_HIDE);
  ShowWindow(Application.MainForm.Handle, SW_HIDE);
end;

procedure TForm1.RestoreMainForm;
var i,j : Integer;
begin
CClose:=false;
timer1.Enabled:=true;
Application.ShowMainForm := True;
ShowWindow(Application.Handle, SW_RESTORE);
ShowWindow(Application.MainForm.Handle, SW_RESTORE);

for I := 0 to Application.MainForm.ComponentCount -1 do
  if Application.MainForm.Components[I] is TWinControl then
    with Application.MainForm.Components[I] as TWinControl do
      if Visible then
        begin
        ShowWindow(Handle, SW_SHOWDEFAULT);
        for J := 0 to ComponentCount -1 do
          if Components[J] is TWinControl then
            ShowWindow((Components[J] as TWinControl).Handle, SW_SHOWDEFAULT);
        end;
end;

procedure TForm1.CreateTrayIcon(n:Integer);
var nidata : TNotifyIconData;
begin
 with nidata do
  begin
   cbSize := SizeOf(TNotifyIconData);
   Wnd := Self.Handle;
   uID := 1;
   uFlags := NIF_ICON or NIF_MESSAGE or NIF_TIP;
   uCallBackMessage := WM_MYICONNOTIFY;
   hIcon := Application.Icon.Handle;
   StrPCopy(szTip,'Статистика 2.0');
  end;
  Shell_NotifyIcon(NIM_ADD, @nidata);
end;

procedure TForm1.DeleteTrayIcon(n:Integer);
var nidata : TNotifyIconData;
begin
 with nidata do
  begin
   cbSize := SizeOf(TNotifyIconData);
   Wnd := Self.Handle;
   uID := 1;
  end;
  Shell_NotifyIcon(NIM_DELETE, @nidata);
end;

procedure TForm1.N1Click(Sender: TObject);
begin
application.Terminate;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
DeleteTrayIcon(1);
end;

procedure TForm1.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
if not CClose
  then
  begin
  canclose:=false
  end
  else
  begin
  Reading;
  s:=ListBox1.Items[ListBox1.Items.Count-1]+'  --  '+datetostr(date)+' '+timetostr(time);
  ListBox1.Items.Delete(ListBox1.Items.Count-1);
  ListBox1.Items.Add(s);
  ListBox1.Items.SaveToFile(ReportFile);
  application.Terminate;
  end;
hidemainform;
end;

procedure TForm1.FormKeyPress(Sender: TObject; var Key: Char);
begin
hidemainform;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
hidemainform;
timer1.Enabled:=false;
cclose:=true;
end;

procedure TForm1.FormClick(Sender: TObject);
begin
hidemainform;
end;

procedure TForm1.Label7Click(Sender: TObject);
begin
ShellExecute(0,pchar(''),pchar('mailto:'+Label7.Caption+'?subject=Statistics 2.0'),pchar(''),pchar(''),1);
hidemainform;
end;

procedure TForm1.Label9Click(Sender: TObject);
begin
ShellExecute(0,pchar(''),pchar(Label9.Caption),pchar(''),pchar(''),1);
hidemainform;
end;

procedure TForm1.N5Click(Sender: TObject);
begin
ShellExecute(0,pchar(''),pchar(ReportFile),pchar(''),pchar(''),1);
end;

end.
